# Docker summary
Docker is a platform for developers to effectively manage their dependecies  
Docker achieves this with help of containers  
containers use less resources than virtualmachines 
## Diffence between Docker and Virtual Machine
![difference](./images/differnce.png )
## A common Docker workflow
![common workflow](./images/dockerwfl.png )
## Essential Docker commands

* `$ docker run hello world`- Docker hello world  
* `$ docker images` - Check number of docker images on your system  
* `$ docker search <image>` – Search an image in the Docker Hub   
* `$ docker run` – Runs a command in a new container.  
* `$ docker start` – Starts one or more stopped containers  
* `$ docker stop` – Stops one or more running containers  
* `$ docker build` – Builds an image form a Docker file  
* `$ docker pull` – Pulls an image or a repository from a registry  
* `$ docker push` – Pushes an image or a repository to a registry  
* `$ docker export` – Exports a container’s filesystem as a tar archive  
* `$ docker exec` – Runs a command in a run-time container  
* `$ docker search` – Searches the Docker Hub for images  
* `$ docker attach` – Attaches to a running container  
* `$ docker commit` – Creates a new image from a container’s changes  

## Docker Image
Docker image contain everythin needed ti run the application as a container, These are
* code.
* Runtime.
* libraries.
* enviornment variables.
* configuration files.
the image can be deployed to any Docker enviornment.
## Container
* Docker container is a runtime instance of an image.
* From one image you can create multiple containers.
## Docker files
* files which contain a set of instruction used to create docker containers,Docker images
## Docker Engine
* it is responsible for the overall functioning of the Docker platform.
* Docker Engine is a client-server based application and consists of 3 main components.
  * Server: It is responsible for creating and managing Docker Images, Containers, Networks and Volumes on the Docker platform.
  * REST API: The REST API specifies how the applications can interact with the Server, and instruct it to get their job done.
  * Client: The Client is nothing but a command-line interface, that allows users to interact with Docker using the commands
## Docker Hub
It is the official online repo where we can find all docker images that are available for use.
