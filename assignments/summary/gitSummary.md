# Git summary
Git is a distributed version control system used to track changes in source code  
It helps programmers to coordinate their work.  
It also help them to go back to a stable program easily in case of any mishaps
## Git installaion
To install git on ubuntu  
 `$ sudo apt-get install git`


To add user name and email  
 `$ git config --global user.name "[ENTER NAME]"  `  
 `$ git config --global user.email "[ENTER EMAIL]"`
 
## Essential git commands
* **init** : used to initialze an existing folder as Git repo  
`$ git init`

* **clone** : used to retirive repo using URL    
`$ git clone url  `

* **status** : shows the modified files in current directory  
`$ git status  `

* **add** : add a file to next commit (stage)  
`$ git add filename `

* **reset** : remove file from (stage)    
`$ git reset filename  `

* **commit** : commit staged content as new commit snapshot  
`$ git commit -m "commit message"`

* **branch** :show / create branch    
`$ git branch [branch name]`

* **checkout** : switch to another branch    
`$ git checkout [branchname]`

* **merge** : merge current branch with another  
`$ git merge branch name`

* **log** : display log of all commits in current branch history  
`$ git log`

* **push** : send local branch commit to remote repo  
`$ git push [branch]`

## Git workflow
![Git Workflow](./images/gitwfl.jpeg)
## Git Branching
![Git branching](./images/gitbrach.jpeg)


